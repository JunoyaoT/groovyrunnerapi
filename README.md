**Groovy Script Runner API**

This is a POC of a spring boot API, which will allow users to submit Groovy code (https://groovy-lang.org/) for execution and get back results of the computation  

---

## Functionalities

- Asynchronous script execution operations
- CRUD operations
- Authentication backed by JWT 
- API reference documentation (Swagger)
- Test for critical parts of the service


---